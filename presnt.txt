\documentclass{beamer} 
\usepackage{times}
\title{SPATIO AND TEMPORAL DATA ANALYSIS OF BIGDATA}
\author[{Anupriya Dominic}]
{\textbf{Anupriya Dominic}\vspace{8pt}\\ \textbf{Anjaly JA}}
\begin{document}
\maketitle
\begin{frame}
\frametitle{Introduction}
\section{}
\begin{itemize}
    \item Big Data is a term used to describe a collection of data that is huge in size and yet growing exponentially with time.
    \item  In short such data is so large and complex that none of the traditional data management tools are able to store it 
\end{itemize}

\end{frame}
\begin{frame}
\frametitle{PROBLEM STATEMENT}
\section{}
Big data storage is a storage infrastructure that is designed specifically to store, manage and retrieve massive amounts of data, or big data.


The data which is related to spatial
(location) and temporal (time) information is called as
spatiotemporal data. 
\begin{itemize}
    \item crime occurrence modelling
    
 \item  ID-based spatiotemporal data
 
 \item  location-based data collected from sensors 
 
\end{itemize}
    
\end{frame}
\begin{frame}
\frametitle{PROPOSED METHODOLOGY}
\section{}
In real world, the attributes of data mostly depends on
location and time. 
    
\end{frame}
\begin{frame}
\frametitle{ALGORITHM}
\begin{itemize}
\item Reconstruct the spatial timeseries s represented by a Vector of AbstractArray states using the embedding struct em of type AbstractSpatialEmbedding.

\item Returns the reconstruction in the form of a Dataset (from DelayEmbeddings) where each row is a reconstructed state and they are ordered first through linear indexing into each state and then incrementing in time.

reconstruct(s::AbstractArray{<:AbstractArray{T,Φ}}, em)
SpatioTemporalEmbedding{Φ,BC,X} → embedding
embedding(rvec, s, t, α)
\item rvec (of length X), reconstructs vector from spatial timeseries s at timestep t and cartesian index α.
\end{itemize}
\end{frame}
\begin{frame}
SpatioTemporalEmbedding{X}(τ, β, bc, fsize)
\begin{itemize}
\item Χ == length(τ) == length(β) : dimensionality of resulting reconstructed space.
\item τ::Vector{Int} = Vector of temporal delays for each entry of the reconstructed space (sorted in ascending order).
\item β::Vector{CartesianIndex{Φ}} = vector of relative indices of spatial delays for each entry of the reconstructed space.
\item bc::BC : boundary condition.
\item fsize::NTuple{Φ, Int} : Size of each state in the timeseries.    
\end{itemize}

    
\end{frame}

\end{document}
